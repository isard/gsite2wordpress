# gsite2wordpress

This tool aims to migrate simple google sites pages to wordpress pages.

## How to install
```
git clone https://gitlab.com/isard/gsite2wordpress.git
cd gsite2wordpress
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## How to run
```
cd gsite_scraping
python3 gsite2wordpress.py GSITE_URL
```

## Help
```
python3 gsite2wordpress.py --help
```

## For developers

### Test suite

gsite2wordpress has a test suite. Tu run tests:

```
python -m unittest tests.test
```

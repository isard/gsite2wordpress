# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.

# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class Common(scrapy.Item):
    """Common elements in all pages."""

    # Site menu
    menu = scrapy.Field()
    # has_banner is True if all site has a custom banner image
    # If site has a banner, it will be downloaded
    # to images store with name banner
    has_banner = scrapy.Field()


class MenuItem(scrapy.Item):
    title = scrapy.Field()
    page = scrapy.Field()
    items = scrapy.Field()


class Page(scrapy.Item):
    """Gsite page."""

    # Name of the page
    name = scrapy.Field()
    # List with each <section> content of a gsite page.
    sections = scrapy.Field()
    # List of all image URLs in this page.
    image_urls = scrapy.Field()


class Section(scrapy.Item):
    """Gsite page section.

    Relevant content of a gsite page is inside a <section> tag.
    """

    # Raw HTML content of a section
    raw_html = scrapy.Field()
    # List of all interesting elements that can be converted
    # to WP blocks inside a section.
    blocks = scrapy.Field()


class Block(scrapy.Item):
    """Element content.

    We need to pick relevant elements that are inside sections.
    Relevant elements are paragraphs, headings, lists...
    """

    # Element type. It can coincide to a: 'p', 'h1', 'ul'...
    type = scrapy.Field()
    # HTML content (including nested elements and nodes if needed)
    content = scrapy.Field()
    # HTML attributes
    style = scrapy.Field()

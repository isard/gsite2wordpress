# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.

# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from slugify import slugify
from urllib.parse import unquote, urlparse


def slug_path(path):
    return slugify(unquote(path))


def slug_url(url):
    return slug_path(urlparse(url).path)

# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.


# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import os

from scrapy.pipelines.images import ImagesPipeline


class ExportHTMLWordpressBlocksPipeline:
    _WP_BLOCK_TAGS = {
        'p': ('<!-- wp:paragraph -->\n'
              '{content}\n'
              '<!-- /wp:paragraph -->'),
        'h1': ('<!-- wp:heading {{"level":1}} -->\n'
               '{content}\n'
               '<!-- /wp:heading -->'),
        'h2': ('<!-- wp:heading -->\n'
               '{content}\n'
               '<!-- /wp:heading -->'),
        'h3': ('<!-- wp:heading {{"level":3}} -->\n'
               '{content}\n'
               '<!-- /wp:heading -->'),
        'h4': ('<!-- wp:heading {{"level":4}} -->\n'
               '{content}\n'
               '<!-- /wp:heading -->'),
        'h5': ('<!-- wp:heading {{"level":5}} -->\n'
               '{content}\n'
               '<!-- /wp:heading -->'),
        'h6': ('<!-- wp:heading {{"level":6}} -->\n'
               '{content}\n'
               '<!-- /wp:heading -->'),
        'ul': ('<!-- wp:list -->\n'
               '{content}\n'
               '<!-- /wp:list -->'),
        'ol': ('<!-- wp:list {{"ordered":true}} -->\n'
               '{content}\n'
               '<!-- /wp:list -->'),
        'img': ('<!-- wp:image {{"sizeSlug":"large"}} -->\n'
                '<figure class="wp-block-image size-large">\n{content}\n'
                '</figure>\n'
                '<!-- /wp:image -->'),
        'video': ('<!-- wp:embed {{"url":"{content}","type":"video",'
                  '"providerNameSlug":"youtube","responsive":true,'
                  '"className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"}} '
                  '-->\n'
                  '<figure class="wp-block-embed is-type-video '
                  'is-provider-youtube wp-block-embed-youtube '
                  'wp-embed-aspect-16-9 wp-has-aspect-ratio">'
                  '<div class="wp-block-embed__wrapper">\n{content}\n</div>'
                  '</figure>\n'
                  '<!-- /wp:embed -->'),
        'google_doc': ('<!-- wp:html -->'
                       '\n<iframe style="{style}" '
                       'src="{content}"></iframe>\n'
                       '<!-- /wp:html -->'),
    }

    def process_item(self, page, spider):
        """ For each gsite page write the equivalent wordpress page."""
        # Create a file for each gsite page.
        self.file = open(os.path.join(
            spider.wp_pages_path, page["name"] + ".html"), 'w')
        # For each relevant gsite element,
        # write the corresponding wordpress block
        for section in page["sections"]:
            for block in section["blocks"]:
                self.file.write(
                    self._WP_BLOCK_TAGS[block['type']].format(
                        content=block['content'],
                        style=block.get('style', '')))
                self.file.write("\n")
        self.file.close()
        return page


class CustomImagesPipeline(ImagesPipeline):

    def file_path(self, request, response=None, info=None, *, item=None):
        """Image filename will be the last part of the image URL."""
        return request.url.split('/')[-1]

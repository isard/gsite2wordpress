# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.


import re
from pathlib import Path
from urllib.parse import urlparse

import requests
from gsite_scraping.items import Common, MenuItem
from gsite_scraping.utils import slug_url
from lxml import html
from scrapy.spiders import Spider


class GSiteCommonSpider(Spider):
    name = "gsite_common"
    start_urls = None
    custom_settings = {
        'ITEM_PIPELINES': {
        }
    }

    def __init__(self, url=None, *args, **kwargs):
        # An URL must be passed by parameter
        # Example:
        #   scrapy crawl gsite_common -a url=https://sites.google.com/view/mygsite
        super().__init__(*args, **kwargs)
        self.start_urls = [url]
        self.allowed_domains = [urlparse(url).netloc]

    def parse(self, response):
        """Get common parts of all pages."""
        common = Common()
        # Banner if exists
        # Get the first background-image in the page
        banner_style = response.xpath(
            '//div[contains(@style, "background-image")]/@style').get()
        if banner_style:
            banner_css = banner_style.split(';')
            for property in banner_css:
                if property.split(':')[0].strip() == 'background-image':
                    common['has_banner'] = True
                    # Get image URL, download the image and
                    # save it in images store
                    background_image = ':'.join(
                        property.split(':')[1:]).strip()
                    image_url = re.search(
                        r'url\((.*)\)', background_image).group(1)
                    image = requests.get(image_url)
                    image_path = Path(
                        self.settings.get(
                            'IMAGES_STORE')) / 'banner'
                    open(image_path, 'wb').write(image.content)
                    break
        else:
            common['has_banner'] = False

        # Menu
        menu_root = response.xpath(
            '//nav/descendant::ul[@role="navigation"]').get()
        if menu_root:
            menu_root_xml = html.fromstring(menu_root)
            common['menu'] = self.create_menu(menu_root_xml)
        else:
            common['menu'] = self.create_menu_one_page(response)
        yield common

    def create_menu(self, root_xml):
        """Scrap menu and save it in objects structure."""
        menuitems = []
        for menu_item_xml in root_xml.iterchildren('li'):
            menu_item = MenuItem()
            menu_item['page'] = slug_url(
                menu_item_xml.find('.//a').get('href'))
            menu_item['title'] = menu_item_xml.find('.//a').text_content()
            if menu_item_xml.find('.//ul') is not None:
                menu_item['items'] = self.create_menu(
                    menu_item_xml.find('.//ul'))
            menuitems.append(menu_item)
        return menuitems

    def create_menu_one_page(self, response):
        page_name = slug_url(response.url)
        title = response.xpath('//h1/text()').get() or page_name
        menu_item = MenuItem()
        menu_item['page'] = page_name
        menu_item['title'] = title
        return [menu_item]

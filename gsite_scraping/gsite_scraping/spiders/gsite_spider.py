# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.


import os
import re
import shutil
from pathlib import Path
from urllib.parse import parse_qs, unquote, urlparse, urlsplit

from gsite_scraping.items import Block, Page, Section
from gsite_scraping.utils import slug_path, slug_url
from lxml import html
from lxml.etree import Element
from lxml.html import clean
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


_XPATHS = {
    'h1': {'type': 'h1', 'content': 'default'},
    'h2': {'type': 'h2', 'content': 'default'},
    'h3': {'type': 'h3', 'content': 'default'},
    'h4': {'type': 'h4', 'content': 'default'},
    'h5': {'type': 'h5', 'content': 'default'},
    'h6': {'type': 'h6', 'content': 'default'},
    'p': {'type': 'p', 'content': 'default'},
    'a[.//p]': {'type': 'p', 'content': 'default'},
    'ul': {'type': 'ul', 'content': 'default'},
    'ol': {'type': 'ol', 'content': 'default'},
    'img': {'type': 'img', 'content': 'default'},
    'iframe[contains(@src,"youtube.com")]': {
        'type': 'video', 'content': 'src'},
    'iframe[contains(@data-src,"docs.google.com")]': {
        'type': 'google_doc', 'content': 'data-src'},
    'iframe[contains(@data-src,"drive.google.com")]': {
        'type': 'google_doc', 'content': 'data-src'},
    'div[contains(@data-url,"docs.google.com")]': {
        'type': 'google_doc', 'content': 'data-url'},
    'div[contains(@data-embed-open-url,"drive.google.com") and not(.//iframe)]': {
        'type': 'google_doc', 'content': 'data-embed-open-url'},
}


def _replace_link(link):
    # Replace gsite links by normalized links for Wordpress site.
    url_split = urlsplit(link)
    if url_split.scheme == '' and not url_split.path.startswith('[images]'):
        # If it is an internal link, get only the page name as the link.
        return '[pages]' + slug_path(link)
    elif url_split.netloc == 'www.google.com' and url_split.path == '/url':
        # If it is an external URL but wrapped by google,
        # return only the external URL.
        # For instance:
        # 'https://www.google.com/url?q=https%3A%2F%2Fxnet.net&amp;sa=D...'
        # will be '"https://xnet.net"
        return parse_qs(url_split.query)['q'][0]
    return link


class GSiteSpider(CrawlSpider):
    name = "gsite"
    start_urls = None
    rules = (Rule(LinkExtractor(), callback='parse_page', follow=True), )
    custom_settings = {
        'FEEDS': {
        }
    }

    def __init__(
        self, url=None, wp_pages_path=Path('pages'),
        *args, **kwargs):
        # An URL must be passed by parameter
        # Example:
        #   scrapy crawl gsite -a url=https://sites.google.com/view/mygsite
        super().__init__(*args, **kwargs)
        self.start_urls = [url]
        self.allowed_domains = [urlparse(url).netloc]

        # Create directory where output pages (WP html) will be stored
        # If already exists will be removed with all its content and
        # created again
        self.wp_pages_path = wp_pages_path
        shutil.rmtree(self.wp_pages_path, ignore_errors=True)
        os.makedirs(self.wp_pages_path)

    def parse_page(self, response):
        """Get relevant information for each gsite page."""
        page = Page()
        # Page name: get gsite name page as the file name
        page["name"] = slug_url(response.url)

        # Get gsite sections
        sections = response.xpath('//section')
        page["sections"] = []

        for site_section in sections:
            section = Section()
            section["raw_html"] = site_section.getall()

            # For each section, get block tags
            blocks = site_section.xpath(self._relevant_elements_xpath())
            section["blocks"] = []

            for site_block in blocks:
                block = Block()
                for xpath, data in _XPATHS.items():
                    # Find what kind of xpath we have
                    # and assign type and content
                    if site_block.xpath('self::' + xpath):
                        block['type'] = data['type']
                        block['style'] = site_block.attrib.get('style', '')
                        # special case: set height to iframes without height
                        if (block['type'] == 'google_doc' and
                            block['style'] == ''):
                            block['style'] = 'height: 500px'
                        content = data['content']
                        if content == 'default':
                            block["content"] = self._prepare_element(
                                block["type"], site_block.xpath('.').get())
                        else:
                            block['content'] = site_block.attrib[content]
                        break
                if block["content"]:
                    section["blocks"].append(block)

            if len(section["blocks"]) > 0:
                page["sections"].append(section)

            # Get image URLs
            raw_image_urls = site_section.xpath('//img/@src').getall()
            clean_image_urls = []
            for img_url in raw_image_urls:
                clean_image_urls.append(response.urljoin(img_url))

        page["image_urls"] = clean_image_urls

        return page

    def _relevant_elements_xpath(self):
        # Build a XPATH to get only elements we are interested on
        super_tags = ['ul', 'ol', 'a']
        only_tags = './/*['
        for xpath in _XPATHS:
            only_tags = only_tags + 'self::' + xpath
            # Exclude elements that are child of a super tag.
            # For instance if an h1 is inside an ul, we won't include it
            # since it is included in the ul element.
            for sbt in super_tags:
                only_tags = (only_tags + ' and not(ancestor::' + sbt + ')')
            only_tags = only_tags + 'or '
        # Remove the last 'or ' and close the claudator
        only_tags = only_tags[:-3] + ']'
        return only_tags

    def _prepare_element(self, type, element):
        # Tidy up element
        element_xml = html.fromstring(element)
        if type == "img":
            # Replace URL by local directory
            element_xml.attrib['src'] = re.sub(
                'https://.*/',
                '[images]',
                element_xml.attrib['src'])
        element_xml = self._clean_attrs(element_xml)
        element_xml = self._remove_empty_tags(element_xml)
        element_xml = self._fix_links(element_xml)
        return unquote(html.tostring(element_xml).decode("utf-8"))

    def _clean_attrs(self, element_xml):
        # Remove all attributes of element excepte safe_attrs.
        safe_attrs = set(self.settings.get('SAFE_ATTRS'))
        cleaner = clean.Cleaner(safe_attrs_only=True, safe_attrs=safe_attrs)
        return cleaner.clean_html(element_xml)

    def _remove_empty_tags(self, element_xml):
        # Remove tags with no text content
        empty_tags = ['img', 'iframe']
        # Store all elements with no text content
        to_remove = [
            c for c in element_xml.iter(tag=Element)
            if c.text_content() == '' and c.tag not in empty_tags]
        # Remove empty elements
        for r in to_remove:
            # If r has no text content but has tail,
            # add the tail to parent element.
            # For instance: <h1>hello <span></span>world!</h1>
            # 'world!' is the tail of <span> element,
            # we will remove span element,
            # but add 'world!' text to <h1> element
            parent = r.getparent()
            if parent:
                parent.text = str(parent.text or '') + str(r.tail or '')
                parent.remove(r)
        return element_xml

    def _fix_links(self, element_xml):
        # Fix all links with Wordpress links
        element_xml.rewrite_links(_replace_link)
        return element_xml

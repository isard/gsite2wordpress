# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.


import os
from pathlib import Path

import click
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor

from gsite_scraping.spiders.gsite_common_spider import GSiteCommonSpider
from gsite_scraping.spiders.gsite_spider import GSiteSpider


@click.command()
@click.argument('url')
@click.option(
    '-o', '--output', required=False,
    type=click.Path(dir_okay=True), default='gsite2wordpress_output',
    help='Save output into PATH.', show_default=True)
def gsite2wordpress(url, output):
    """Convert a Google Site to Wordpress pages.

    gsite2wordpress creates 'gsite2wordpress_output' directory with two subdirectories:
    'pages' with all Wordpress HTML pages and 'images' with all images.
    'gsite2wordpress_output' name can be changed using -o option.
    """
    configure_logging()
    # Set root path for images store path
    settings = get_project_settings()
    settings.set('IMAGES_STORE', str(Path(output) / 'images') + os.sep)
    settings.set(
        'FEEDS',
        {str(Path(output) / 'common.json'): {'format': 'json', 'indent': 2}})

    # Start craling passing to our spider gsite URL and output path
    runner = CrawlerRunner(settings)
    runner.crawl(
      GSiteSpider,
      url=url,
      wp_pages_path=str(Path(output) / 'pages'))
    runner.crawl(GSiteCommonSpider, url=url)
    d = runner.join()
    d.addBoth(lambda _: reactor.stop())

    reactor.run()


if __name__ == '__main__':
    gsite2wordpress()

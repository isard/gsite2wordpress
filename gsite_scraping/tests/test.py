# Copyright 2022 Mònica Ramírez Arceda

# This file is part of gsite2wordpress.

# gsite2wordpress is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# gsite2wordpress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with gsite2wordpress. If not, see <https://www.gnu.org/licenses/>.


import filecmp
import os
import shutil
import unittest
from pathlib import Path

from gsite_scraping.pipelines import ExportHTMLWordpressBlocksPipeline
from gsite_scraping.spiders.gsite_spider import GSiteSpider
from scrapy.crawler import Crawler
from scrapy.http import HtmlResponse, Request

RESULTS_PATH = Path(__file__).parent.resolve() / 'results'
FIXTURES_PATH = Path(__file__).parent.resolve() / 'fixtures'
GSITE_PATH = FIXTURES_PATH / 'gsite'
WP_PATH = FIXTURES_PATH / 'wordpress'


def fake_response_from_file(file_name):
    """
    Create a Scrapy fake HTTP response from a HTML file.
    @param file_name: filename to convert ro a fake response.
    returns: A scrapy HTTP response.
    """
    url = 'http://fake.net/' + str(Path(file_name).stem)
    request = Request(url=url)
    with open(file_name, 'r') as f:
        file_content = f.read()
        response = HtmlResponse(
            url=url, request=request, body=file_content, encoding='utf-8')

    return response


class GSiteSpiderTest(unittest.TestCase):

    def setUp(self):
        settings = {
            'IMAGES_STORE': 'images/',
            'SAFE_ATTRS': ['src', 'alt', 'href', 'title', 'width', 'height']
        }
        crawler = Crawler(GSiteSpider, settings=settings)
        crawler.crawl()
        crawler.spider.wp_pages_path = RESULTS_PATH
        self.spider = crawler.spider
        self.pipeline = ExportHTMLWordpressBlocksPipeline()
        os.mkdir(RESULTS_PATH)

    def test_parse(self):
        # Loop all gsite html files and convert them to wordpress html files
        for test_page in os.listdir(GSITE_PATH):
            print("Testing " + test_page)
            page = self.spider.parse_page(fake_response_from_file(
                GSITE_PATH / test_page))
            self.pipeline.process_item(page, self.spider)
            # Test converted file is the expected
            self.assertTrue(filecmp.cmp(
                WP_PATH / test_page, RESULTS_PATH / test_page))

    def tearDown(self):
        super().tearDown()
        shutil.rmtree(RESULTS_PATH)
